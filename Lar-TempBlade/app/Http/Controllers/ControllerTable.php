<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControllerTable extends Controller
{
    public function ShowTable() {
        return view('table');
    }

    public function ShowDataTable() {
        return view('datatable');
    }
}
