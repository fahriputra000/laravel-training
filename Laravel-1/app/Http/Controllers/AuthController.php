<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata() {
        return view('/page/form');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $gender = $request['gender'];
        $Nationality = $request['Nationality'];
        $language_spoken = $request['language_spoken'];
        return view('/page/welcome',compact('first_name', 'last_name','gender', 'Nationality', 'language_spoken'));
    }
}
