<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ControllerMaster@MasterIndex');
// CastCRUD
Route::get('/cast','CastController@Index');
Route::get('/cast/create','CastController@Create');
Route::post('/cast','CastController@Store');
Route::get('/cast/{cast_id}','CastController@Show');
Route::get('/cast/{cast_id}/edit','CastController@Edit');
Route::put('/cast/{cast_id}','CastController@Update');
Route::delete('/cast/{cast_id}','CastController@Destroy');