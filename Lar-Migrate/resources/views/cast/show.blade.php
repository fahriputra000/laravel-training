@extends('master')
@section('Title')
    Show Cast {{$cast->id}}
@endsection
@section('content')
<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="..." alt="Card image cap">
    <div class="card-body">
      <p class="card-text">Nama : {{$cast->nama}}<br>Umur : {{$cast->umur}}<br>Bio : {{$cast->bio}}</p>
    </div>
  </div>
@endsection